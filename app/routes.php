<?php
/** Routes **/

$app->get('mvc/addresses', 'AddressController@index');
$app->get('mvc/add-address', 'AddressController@create');
$app->post('mvc/add-address', 'AddressController@store');
$app->get('mvc/address/{id}', 'AddressController@show');
$app->get('mvc/address/{id}/edit', 'AddressController@edit');
$app->put('mvc/address/{id}', 'AddressController@update');
$app->delete('mvc/address/{id}', 'AddressController@destroy');

/** Routes **/