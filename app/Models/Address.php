<?php namespace app\Models;

use system\Model;

class Address extends Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function addAddress($params)
    {
        if($query = $this->insert('addresses', $params))
            return $query;
        else
            return false;
    }

    public function getAllAddresses()
    {
        return $this->getAll('addresses');
    }

    public function getAddress($id)
    {
        return $this->getOne('addresses',['id' => $id]);
    }

    public function updateAddress($id, $array)
    {
        return $this->updateById('addresses',$id, $array);
    }

    public function deleteAddress($id)
    {
        return $this->deleteById('addresses',$id);
    }
    
}