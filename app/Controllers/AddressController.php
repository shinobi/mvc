<?php namespace app\Controllers;

use system\Controller;
use system\Library\Response;

class AddressController extends Controller{

    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->model = $this->load->model('Address'); // Load model for this controller
    }
    
    public function index()
    {
        $array = [];
        $addresses = $this->model->getAllAddresses();
        foreach($addresses as $address)
        {
            $array[] = [
                'id' => $address['id'],
                'name' => $address['name'],
                'address' => $address['address'],
                'telephone' => $address['telephone']
            ];
        }

        return Response::getInstance()->setData($array)->send();
    }

    public function create()
    {
        $this->load->view('addresses/create');
    }

    public function store()
    {
        $inputs = $this->request->all();
        if($inputs['name']=="" and $inputs['address']=="" and $inputs['telephone']=="")
            return Response::getInstance()->setStatus(404)->setData(["error" => "All fields are required", "code" => 404])->send();

        $params = array (
            'name' => $this->request->input('name'),
            'address' => $this->request->input('address'),
            'telephone' => $this->request->input('telephone')
        );

        if($this->model->addAddress($params))
        {
            return Response::getInstance()
                ->setStatus(200)
                ->setData([
                    "success" => "Address was added",
                    "code" => 200
                ])->send();
        }
        else
        {
            return Response::getInstance()
                ->setStatus(503)
                ->setData([
                    "error" => "DB Error: Address couldn't be added",
                    "code" => 503
                ])->send();
        }
    }

    public function show($id)
    {
        $query = $this->model->getAddress($id);
        if(!is_array($query))
            return Response::getInstance()->setStatus(400)->setData(['error' => 'Record not found', 'code' => 400])->send();

        $data = [
            'id' => $query['id'],
            'name' => $query['name'],
            'address' => $query['address'],
            'telephone' => $query['telephone']
        ];

        return Response::getInstance()->setData($data)->send();
        
    }

    public function edit($id)
    {
        $query = $this->model->getAddress($id);

        $data = [
            'id' => $query['id'],
            'name' => $query['name'],
            'address' => $query['address'],
            'telephone' => $query['telephone']
        ];
        $this->load->view('addresses/edit', $data);
    }

    public function update($id)
    {
        $inputs = $this->request->all();
        if($inputs['name']=="" and $inputs['address']=="" and $inputs['telephone']=="")
            return Response::getInstance()->setStatus(400)->setData(["error" => "All fields are required", "code" => 400])->send();

        $params = array (
            'name' => $this->request->input('name'),
            'address' => $this->request->input('address'),
            'telephone' => $this->request->input('telephone')
        );

        if($this->model->updateAddress($id, $params))
        {

            return Response::getInstance()->setStatus(200)
                ->setData([
                    "success" => "Address was updated",
                    "code" => 200
                ])->send();
        }
        else
        {
            return Response::getInstance()->setStatus(503)
                ->setData([
                    "error" => "DB Error: Address couldn't be updated",
                    "code" => 503
                ])->send();
        }
    }

    public function destroy($id)
    {
        if($this->model->deleteAddress($id))
        {
            return Response::getInstance()->setStatus(200)
                ->setData([
                    "success" => "Address was deleted",
                    "code" => 200
                ])->send();
        }
        else
        {
            return Response::getInstance()->setStatus(503)
                ->setData([
                    "error" => "DB Error: Address couldn't be deleted",
                    "code" => 503
                ])->send();
        }
    }
}
