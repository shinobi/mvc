<?php
/*
 * Requirements for db connection
 */
return [
    'host' => 'localhost',
    'dbname' => 'addresses',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8'
];