<?php namespace system;

use system\Library\Bootstrap as Bootstrap;
use system\Library\Route as Route;

class App
{
    private $route, $bootstrap;

    public function __construct()
    {
        $this->route = Route::getRoute(); // Get route instance
        $this->bootstrap = new Bootstrap($this->route); // Get bootstrap instance
    }

    public function get($url, $controller)
    {
        $this->route->add($url, 'get', $controller); // add route to getList
    }

    public function post($url, $controller)
    {
        $this->route->add($url, 'post', $controller); // add route to postList
    }

    public function put($url, $controller)
    {
        $this->route->add($url, 'put', $controller); // add route to putList
    }

    public function delete($url, $controller)
    {
        $this->route->add($url, 'delete', $controller); // add route to deleteList
    }

    public function run()
    {
        echo $this->bootstrap->getController();
    }

}