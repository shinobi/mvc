<?php namespace system;

use system\Library\Database;

class Model {

    protected $db;

    public function __construct()
    {
        $this->db = Database::getConnection()->db;
    }

    /**
     * Insert method for db transactions
     * @param $table
     * @param $array
     * @return bool
     */
    public function insert($table, $array)
    {
        $columns = implode(',',array_keys($array));
        $values = implode(',', array_fill(0, count(array_keys($array)), '?'));

        $sql = $this->db->prepare('INSERT INTO '.$table.' ('.$columns.') VALUES ('.$values.')');
        $query = $sql->execute(array_values($array));

        return $query;
    }

    /**
     * Select all data from the table given
     * @param $table
     * @return \PDOStatement
     */
    public function getAll($table)
    {
        $query = $this->db->prepare('SELECT * FROM '.$table);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Select one row from the table given with where clause(s)
     * @param $table
     * @param $array
     * @return mixed
     */
    public function getOne($table, $array)
    {
        $where = "WHERE"; $i = 1;
        foreach($array as $key => $value)
        {
            $where .= ' '.$key.' = ?';
            if($i != count($array))
                $where .= ",";
            $i++;
        }

        $query = $this->db->prepare('SELECT * FROM '.$table.' '.$where.' LIMIT 1');
        $query->execute(array_values($array));
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Update row with its primary key which is "id" 
     * @param $table
     * @param $id
     * @param $array
     * @return bool
     */
    public function updateById($table, $id, $array)
    {
        $columns = ""; $y = 1;
        foreach($array as $a => $b)
        {
            $columns .= ' '.$a.' = ?';
            if($y != count($array))
                $columns .= ",";
            $y++;
        }

        $sql = $this->db->prepare('UPDATE '.$table.' SET '.$columns.' WHERE id = '.$id.' LIMIT 1');

        $query = $sql->execute(array_values($array));

        return $query;

    }

    /**
     * Delete row with its primary key which is "id"
     * @param $table
     * @param $id
     * @return bool
     */
    public function deleteById($table, $id)
    {
        $sql = $this->db->prepare('DELETE FROM '.$table.' WHERE id=? LIMIT 1');
        $query = $sql->execute([$id]);
        
        return $query;
    }
}