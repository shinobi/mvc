<?php namespace system\Library;

class Route {

    private $getList = [], $postList = [], $putList = [], $deleteList = [];
    private static $instance = NULL;
    public $controller, $parameter = [];

    private function __construct(){
    }

    public static function getRoute()
    {
        if(NULL == self::$instance)
            self::$instance = new Route();

        return self::$instance;
    }

    /**
     * This method adds all given routes to the given method lists
     * @param $url
     * @param $method
     * @param $controller
     */
    public function add($url, $method, $controller)
    {
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        $url = array_map('trim', $url);

        $array = [
            'url' => $url,
            'controller' => $controller
        ];

        switch ($method){
            case 'get' : array_push($this->getList, $array); break;
            case 'post': array_push($this->postList, $array); break;
            case 'put': array_push($this->putList, $array); break;
            case 'delete': array_push($this->deleteList, $array); break;
        }
    }

    /**
     * This method checks if the requested uri is in route lists
     * @param $uri
     * @param $method
     * @return bool
     * @throws \Exception
     */
    public function check($uri, $method)
    {
        switch ($method){
            case 'get' : $array = $this->getList; break;
            case 'post': $array = $this->postList; break;
            case 'put': $array = $this->putList; break;
            case 'delete': $array = $this->deleteList; break;
            default: $array = $this->getList; break;
        }

        //dd($uri);

        $uriCount = count($uri);

        $i = 0;
        foreach($array as $a)
        {
            if($i === $uriCount) // if match completed then break; don't check for the other route lists
                break;

            if(count($a['url']) === $uriCount)
            {
               $i = 0;
               for($y = 0; $y < $uriCount; $y++)
               {
                    if($a['url'][$y] == $uri[$y])
                        $i++;
                    else
                        if(preg_match('/\{(.*?)\}/i', $a['url'][$y], $result)) // if there's a {parameter} in route
                        {
                            $this->parameter[] = $uri[$y];
                            $i++;
                        }
                        else
                            continue;

                    if($i === $uriCount ) // if match completed then get controller name from route list
                        $this->controller = $a['controller'];
               }
            }
            else
               continue;

        }

        if($i === $uriCount)
            return true;
        else
            throw new \Exception('Route not found');

    }
}