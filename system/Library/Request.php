<?php namespace system\Library;

class Request {

    private $method;
    public static $instance=null;

    public function __construct()
    {
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
    }

    public static function getRequest()
    {
        if (is_null(self::$instance))
            self::$instance = new static();

        return self::$instance;
    }

    public function input($post = false)
    {
        if($post)
        {
            if(isset($_POST[$post]))
                return $_POST[$post];
            return $_GET[$post];
        }
        else
            return null;
    }

    public function all()
    {
        if(isset($_POST))
            return $_POST;
        return $_GET;
    }

    public function getMethod()
    {
        if(isset($_POST['_method'])) // if there is _method input in a form, then overwrite request method
           $this->method = strtolower($_POST['_method']); // Overwrite method

        return $this->method;
    }
    
}