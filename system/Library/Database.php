<?php namespace system\Library;

class Database
{
    public $db;
    private static $instance = NULL;

    /**
     * Database constructor.
     * Connect to database and assign instance to a public object
     */
    public function __construct()
    {
        $config = Config::get('database');

        try {
            /** TODO 
             * dbname value should be taken from a config file
             */
            $this->db = new \PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'].';charset='.$config['charset'].'', $config['username'], $config['password']);
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    public static function getConnection()
    {
        if( NULL == self::$instance)
            self::$instance = new Database();
        
        return self::$instance;
    }

}