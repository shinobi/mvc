<?php namespace system\Library;

class Bootstrap {
    
    private $url, $route, $output;

    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * This method gets the request uri and returns parsed url in array format
     * @return mixed
     */
    private function getParsedUrl()
    {
        $url = trim($_SERVER['REQUEST_URI'], '/');
        $url = explode('/', $url);
        $this->url = array_map('trim', $url);
        return $this->url;
    }

    /**
     * This method parses any given data with a delimiter.
     * In this case we use it to parse controller and method names 
     * match with the route given in index.php
     * @param $param
     * @param string $delimiter
     * @return mixed
     */
    private function parseData($param, $delimiter = '@')
    {
        return explode($delimiter, $param);
    }

    public function getController()
    {

        try {
            /* Check If the given url is in our route list */
            if ($this->route->check($this->getParsedUrl(), Request::getRequest()->getMethod())) {

                $array = $this->parseData($this->route->controller); // Get parsed controller for the matched route
                
                if(file_exists(  __DIR__ .'/../../app/Controllers/'.$array[0].'.php' )) // Check if controller file exists
                {
                    $ns = 'app\Controllers\\'.$array[0];
                    $class = new $ns; // Get instance from controller which is given in route

                    if(count($p = $this->route->parameter) > 0) // If parameters exist for the method
                    {
                        $this->output = call_user_func_array([$class, $array[1]], $this->route->parameter);
                    }
                    else
                        $this->output = $class->{$array[1]}();

                    return $this->output;
                }
                else
                    die('Controller Not Found');
            }
        } catch (\Exception $e) {

            die($e->getMessage());
        }
    }
    
}