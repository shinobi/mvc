<?php namespace system\Library;

class Config
{
    public static $file = [];

    /**
     * Get config file if it exists
     * @param $config
     * @return array|mixed
     * @throws \Exception
     */
    public static function get($config)
    {
        $config = strtolower($config);
        if(file_exists( __DIR__.'/../../app/config/'.$config.'.php'))
            self::$file = include ( __DIR__.'/../../app/config/'.$config.'.php');
        else
            throw new \Exception('Config file not found');
        
        return self::$file;
    }
    
}