<?php namespace system\Library;

class Response
{
    private static $instance = NULL;
    private $status = 200, $data;

    public static function getInstance()
    {
        if(is_null(self::$instance))
            self::$instance = new Response();
        
        return self::$instance;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this->getInstance();
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this->getInstance();
    }

    public function send()
    {
        http_response_code($this->status);
        echo json_encode($this->data);
    }
}