<?php namespace system\Library;

class Loader {

    /**
     * This method loads the given model and create an instance from it
     * @param $model
     * @return mixed
     */
    public function model($model)
    {
        $ns = '\app\Models\\'.$model;

        $class = new $ns;
        
        return $class;
    }


    /**
     * This method loads view (html) file and 
     * sends the parameters given in array format to the view file
     * @param $file
     * @param null $array
     */
    public function view($file, $array = null)
    {
        if(is_array($array))
            foreach($array as $key => $value)
                $$key = $value;

        require_once (__DIR__. '/../../app/Views/'.$file.'.php');
    }
}