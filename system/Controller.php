<?php namespace system;

use system\Library\Loader;
use system\Library\Request;

class Controller {
    
    protected $load, $request;
    
    public function __construct()
    {
        $this->load = new Loader();
        $this->request = Request::getRequest();
    }
}